let initialState = {
    loading: false
  };
  
  const loadingReducer = (state = initialState, action) => {
    if (action.type === "TOGGLE_LOADER") {
      return {
        loading: action.status
      };
    } else {
      return state;
    }
  };
  
  export default loadingReducer;
  