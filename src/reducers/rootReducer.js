import allDataReducer from "./allDataReducer";
import playerDataReducer from "./playerDataReducer";
import loadingReducer from "./loadingReducer";
import analystReducer from "./analystReducer";
import redditReducer from "./redditReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  allData: allDataReducer,
  playerData: playerDataReducer,
  loader: loadingReducer,
  analysts: analystReducer,
  reddit: redditReducer,
});

export default rootReducer;
