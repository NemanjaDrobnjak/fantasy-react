let initialState = {
  teams: [],
  players: [],
  fixtures: []
};

const allDataReducer = (state = initialState, action) => {
  if (action.type === "LOAD_MAIN_DATA") {
    return {
      teams: action.teams,
      players: action.players,
      fixtures: action.fixtures
    };
  } else {
    return state;
  }
    
};

export default allDataReducer;
