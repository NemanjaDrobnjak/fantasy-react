let initialState = {
    analyst1: {},
    analyst2: {},
    analyst3: {},
    analyst4: {},
    analyst5: {}
  };
  
  const analystReducer = (state = initialState, action) => {
    if (action.type === "LOAD_ALL_ANALYSTS") {
      return {
        analyst1: action.analyst1,
        analyst2: action.analyst2,
        analyst3: action.analyst3,
        analyst4: action.analyst4,
        analyst5: action.analyst5
      };
    } else {
      return state;
    }
  };
  
  export default analystReducer;
  