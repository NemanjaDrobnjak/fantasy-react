let initialState = {
  player1stats: {
    year2018: {},
    year2019: {},
    nextFixtures: [],
  },
  player2stats: {
    year2018: {},
    year2019: {},
    nextFixtures: [],
  },
  player3stats: {
    year2018: {},
    year2019: {},
    nextFixtures: [],
  },
  player4stats: {
    year2018: {},
    year2019: {},
    nextFixtures: [],
  },
};

const playerDataReducer = (state = initialState, action) => {
  if (action.type === "LOAD_PLAYER_DATA") {
    console.log("ACTION", action);
    return {
      player1stats: action.player1stats,
      player2stats: action.player2stats,
      player3stats: action.player3stats,
      player4stats: action.player4stats,
    };
  } else {
    return state;
  }
};

export default playerDataReducer;
