let initialState = {
    posts: []
  };
  
  const redditReducer = (state = initialState, action) => {
    if (action.type === "LOAD_REDDIT_FEED") {
      return {
        posts: action.posts
      };
    } else {
      return state;
    }
  };
  
  export default redditReducer;
  