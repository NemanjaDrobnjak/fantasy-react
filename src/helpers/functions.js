import uuid from "uuid-random";

export const addPointsAndNextFixturesToTeams = (fixtures, teams) => {
  // Add new props to team object
  for (let team of teams) {
    team.goal_scored = 0;
    team.goal_received = 0;
    team.goal_diff = 0;
    team.players = [];
    team.nextFixtures = [];
  }
  for (let fixture of fixtures) {
    // First check if the game finished
    if (fixture.finished === true) {
      let scoreH = fixture.team_h_score;
      let scoreA = fixture.team_a_score;
      for (let team of teams) {
        // Add stats to the away team
        if (fixture.team_a === team.id) {
          team.goal_scored += scoreA;
          team.goal_received += scoreH;
          team.goal_diff = team.goal_scored - team.goal_received;
          if (scoreA > scoreH) {
            team.win++;
            team.points += 3;
          } else if (scoreA === scoreH) {
            team.draw++;
            team.points += 1;
          } else {
            team.loss++;
          }
        }
        // Add stats to the home team
        if (fixture.team_h === team.id) {
          team.goal_scored += scoreH;
          team.goal_received += scoreA;
          team.goal_diff = team.goal_scored - team.goal_received;
          if (scoreH > scoreA) {
            team.win++;
            team.points += 3;
          } else if (scoreA === scoreH) {
            team.draw++;
            team.points += 1;
          } else {
            team.loss++;
          }
        }
      }
    } else {
      for (let team of teams) {
        if (team.id === fixture.team_h) {
          let awayTeam = teams.find((element) => element.id === fixture.team_a);
          let ratingText = "";
          switch(fixture.team_h_difficulty) {
            case 2:
              ratingText = "easy";
              break;
            case 3:
              ratingText = "medium";
              break;
            case 4:
              ratingText = "hard";
              break;
            default:
              ratingText = "medium";
              break;
          }
          team.nextFixtures.push({
            name: awayTeam.short_name,
            stadium: "A",
            rating: ratingText,
            id: fixture.id,
          });
        } else if (team.id === fixture.team_a) {
          let homeTeam = teams.find((element) => element.id === fixture.team_h);
          let ratingText = "";
          switch(fixture.team_a_difficulty) {
            case 2:
              ratingText = "easy";
              break;
            case 3:
              ratingText = "medium";
              break;
            case 4:
              ratingText = "hard";
              break;
            default:
              ratingText = "medium";
              break;
          }
          team.nextFixtures.push({
            name: homeTeam.short_name,
            stadium: "H",
            rating: ratingText,
            id: fixture.id,
          });
        }
      }
    }
  }

  for (let team of teams) {
    // Sort teams next fixtures
    team.nextFixtures.sort(function (a, b) {
      return a.id - b.id;
    });
  }

  
  // Sort teams by points
  teams.sort(function (a, b) {
    return b.points - a.points;
  });

  // Add next fixtures to teams

  return teams;
};

export const addPlayersToTeams = (players, teams) => {
  for (let player of players) {
    for (let team of teams) {
      if (player.team === team.id) {
        team.players.push(player);
      }
    }
  }

  return teams;
};

export const getUniqueKey = () => {
  return uuid();
};

export const twitterAccounts = (teamName) => {
  switch (teamName) {
    case "Everton":
      return "Everton";
    case "Aston Villa":
      return "AVFCOfficial";
    case "Liverpool":
      return "LFC";
    case "Arsenal":
      return "Arsenal";
    case "Leicester":
      return "LCFC";
    case "Wolves":
      return "Wolves";
    case "Chelsea":
      return "ChelseaFC";
    case "Spurs":
      return "SpursOffical";
    case "Crystal Palace":
      return "CPFC";
    case "Leeds":
      return "LUFC";
    case "Man City":
      return "ManCity";
    case "Newcastle":
      return "NUFC";
    case "Southampton":
      return "SouthamptonFC";
    case "West Ham":
      return "WestHam";
    case "Man Utd":
      return "ManUtd";
    case "Brighton":
      return "OfficialBHAFC";
    case "West Brom":
      return "WBA";
    case "Burnley":
      return "BurnleyOffical";
    case "Fulham":
      return "Fulham";
    case "Sheffield Utd":
      return "SheffieldUnited";
    default:
      return "premierleague";
  }
};
