import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";

import { Provider } from "react-redux";
import configureStore from "./configureStore";

import "./scss/main.scss";
import Home from "./components/Home.js";

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route path="/" component={Home} />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);
