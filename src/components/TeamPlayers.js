import React from "react";

import { Timeline } from "react-twitter-widgets";

import { twitterAccounts } from "../helpers/functions";

class TeamPlayers extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  render() {
    return (
      <div className="teams__container">
        <div className="teams__list">
          <div className="team__row">
            <div className="column__header">Index</div>
            <div className="column__header">Name</div>
            <div className="column__header">Form</div>
            <div className="column__header">Price</div>
            <div className="column__header">Selected</div>
            <div className="column__header">GW points</div>
            <div className="column__header">Goals Scored</div>
            <div className="column__header">Assists</div>
            <div className="column__header">Total Points</div>
          </div>
          {this.props.location.state.team.players &&
            this.props.location.state.team.players.map((player) => {
              return (
                <div className="team__row" onClick={() => {}}>
                  <div className="row__entry">
                    {this.props.location.state.team.players.indexOf(player) + 1}
                  </div>
                  <div className="row__entry">{player.web_name}</div>
                  <div className="row__entry">{player.form}</div>
                  <div className="row__entry">
                    £{(player.now_cost / 10).toFixed(1)}
                  </div>
                  <div className="row__entry">
                    {player.selected_by_percent}%
                  </div>
                  <div className="row__entry">{player.event_points}</div>
                  <div className="row__entry">{player.goals_scored}</div>
                  <div className="row__entry">{player.assists}</div>
                  <div className="row__entry">{player.total_points}</div>
                </div>
              );
            })}
        </div>
        <div className="team__social">
          <Timeline
            dataSource={{
              sourceType: "profile",
              screenName: twitterAccounts(this.props.location.state.team.name),
            }}
            options={{ theme: "dark", width: "400", height: "600" }}
          />
        </div>
      </div>
    );
  }
}

export default TeamPlayers;
