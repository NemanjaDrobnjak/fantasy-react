import React from "react";

class Player extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <div className="left__panel">
          <div className="player__info">
            <div className="info__left">
              {this.props.activePlayer.second_name}{" "}
              {this.props.activePlayer.first_name}
            </div>
            <div className="info__right">
                {this.props.teams.find(team => team.id === this.props.activePlayer.team).name}
            </div>
          </div>
          <div className="player__stats"></div>
          <div className="player__ict"></div>
          <div className="player__results-fixtures"></div>
        </div>
        <div className="right__panel"></div>
      </div>
    );
  }
}

export default Player;
