import React from "react";

import { NavLink } from "react-router-dom";

const Header = (props) => {
  return (
    <div className="header__wrapper">
      <NavLink to="/" className="app__title">
        Fantasy Tool by Nemanja Drobnjak
      </NavLink>
      <NavLink
        exact
        to="/"
        className="header__button-home"
        activeClassName="header__button-home active"
      >
        Home
      </NavLink>
      {/* <NavLink to="/allplayers" className="header__button-allplayers">
        All Players
      </NavLink> */}
      <NavLink
        to="/compare"
        className="header__button-compare"
        activeClassName="header__button-compare active"
      >
        Compare
      </NavLink>
      <NavLink
        to="/analysts"
        className="header__button-analysts"
        activeClassName="header__button-analysts active"
      >
        Analysts
      </NavLink>
      <NavLink
        to="/social"
        className="header__button-social"
        activeClassName="header__button-social active"
      >
        Social
      </NavLink>
    </div>
  );
};

export default Header;
