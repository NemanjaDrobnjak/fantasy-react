import React from "react";

const CompareSearch = (props) => {
  return (
    <div className="compare__search-container">
      <div className="compare__search">
        <input
          className="search__field"
          placeholder="Search player..."
          onChange={(event) => props.handleSearchChange(event)}
        ></input>
      </div>
      <div className="compare__player-list">
        {props.players &&
          props.players.map((player) => {
            return (
              <div
                className="compare__player-row"
                onClick={() => {
                  props.playerClick(player);
                }}
              >
                {player.web_name}
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default CompareSearch;
