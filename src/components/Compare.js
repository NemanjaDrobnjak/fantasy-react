import React from "react";

import CompareSearch from "./CompareSearch";
import ComparePlayer from "./ComparePlayer";

class Compare extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePlayer1: {},
      activePlayer2: {},
      activePlayer3: {},
      activePlayer4: {},
      search1Active: true,
      search2Active: true,
      search3Active: true,
      search4Active: true,
      playerList1: props.players,
      playerList2: props.players,
      playerList3: props.players,
      playerList4: props.players,
      playerListOriginal: props.players,
    };

    this.playerClick1 = this.playerClick1.bind(this);
    this.playerClick2 = this.playerClick2.bind(this);
    this.playerClick3 = this.playerClick3.bind(this);
    this.playerClick4 = this.playerClick4.bind(this);
    this.closePlayerWindow1 = this.closePlayerWindow1.bind(this);
    this.closePlayerWindow2 = this.closePlayerWindow2.bind(this);
    this.closePlayerWindow3 = this.closePlayerWindow3.bind(this);
    this.closePlayerWindow4 = this.closePlayerWindow4.bind(this);
    this.handleSearchChange1 = this.handleSearchChange1.bind(this);
    this.handleSearchChange2 = this.handleSearchChange2.bind(this);
    this.handleSearchChange3 = this.handleSearchChange3.bind(this);
    this.handleSearchChange4 = this.handleSearchChange4.bind(this);
  }

  // TODO: OPTIMIZE FUNCTIONS AND ARCHITECTURE OF THE COMPARE COMPONENT

  playerClick1(player) {
    this.props.loadPlayerData(player.id, 1);
    //Changes active player in state
    this.setState({
      activePlayer1: player,
      search1Active: false,
    });
  }

  playerClick2(player) {
    this.props.loadPlayerData(player.id, 2);
    //Changes active player in state
    this.setState({
      activePlayer2: player,
      search2Active: false,
    });
  }

  playerClick3(player) {
    this.props.loadPlayerData(player.id, 3);
    //Changes active player in state
    this.setState({
      activePlayer3: player,
      search3Active: false,
    });
  }

  playerClick4(player) {
    this.props.loadPlayerData(player.id, 4);
    //Changes active player in state
    this.setState({
      activePlayer4: player,
      search4Active: false,
    });
  }

  closePlayerWindow1() {
    //Closes ComparePlayerComponent and activates search again
    this.setState({
      search1Active: true,
      playerList1: this.state.playerListOriginal,
    });
  }

  closePlayerWindow2() {
    //Closes ComparePlayerComponent and activates search again
    this.setState({
      search2Active: true,
      playerList2: this.state.playerListOriginal,
    });
  }

  closePlayerWindow3() {
    //Closes ComparePlayerComponent and activates search again
    this.setState({
      search3Active: true,
      playerList3: this.state.playerListOriginal,
    });
  }

  closePlayerWindow4() {
    //Closes ComparePlayerComponent and activates search again
    this.setState({
      search4Active: true,
      playerList4: this.state.playerListOriginal,
    });
  }

  handleSearchChange1(event) {
    let updatedPlayerList1 = this.state.playerListOriginal.filter((player) => {
      return (
        player.first_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase()) ||
        player.second_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase())
      );
    });
    this.setState({
      playerList1: updatedPlayerList1,
    });
  }

  handleSearchChange2(event) {
    let updatedPlayerList2 = this.state.playerListOriginal.filter((player) => {
      return (
        player.first_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase()) ||
        player.second_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase())
      );
    });
    this.setState({
      playerList2: updatedPlayerList2,
    });
  }

  handleSearchChange3(event) {
    let updatedPlayerList3 = this.state.playerListOriginal.filter((player) => {
      return (
        player.first_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase()) ||
        player.second_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase())
      );
    });
    this.setState({
      playerList3: updatedPlayerList3,
    });
  }

  handleSearchChange4(event) {
    let updatedPlayerList4 = this.state.playerListOriginal.filter((player) => {
      return (
        player.first_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase()) ||
        player.second_name
          .toLowerCase()
          .includes(event.target.value.toLowerCase())
      );
    });
    this.setState({
      playerList4: updatedPlayerList4,
    });
  }

  render() {
    return (
      <div className="compare__container">
        <div className="compare__player1">
          {this.state.search1Active ? (
            <CompareSearch
              players={this.state.playerList1}
              playerClick={this.playerClick1}
              handleSearchChange={this.handleSearchChange1}
            />
          ) : (
            <ComparePlayer
              teams={this.props.teams}
              activePlayer={this.state.activePlayer1}
              activePlayerStats={this.props.player1stats}
              closePlayerWindow={this.closePlayerWindow1}
            />
          )}
        </div>
        <div className="compare__player2">
          {this.state.search2Active ? (
            <CompareSearch
              players={this.state.playerList2}
              playerClick={this.playerClick2}
              handleSearchChange={this.handleSearchChange2}
            />
          ) : (
            <ComparePlayer
              teams={this.props.teams}
              activePlayer={this.state.activePlayer2}
              activePlayerStats={this.props.player2stats}
              closePlayerWindow={this.closePlayerWindow2}
            />
          )}
        </div>
        <div className="compare__player3">
          {this.state.search3Active ? (
            <CompareSearch
              players={this.state.playerList3}
              playerClick={this.playerClick3}
              handleSearchChange={this.handleSearchChange3}
            />
          ) : (
            <ComparePlayer
              teams={this.props.teams}
              activePlayer={this.state.activePlayer3}
              activePlayerStats={this.props.player3stats}
              closePlayerWindow={this.closePlayerWindow3}
            />
          )}
        </div>
        <div className="compare__player4">
          {this.state.search4Active ? (
            <CompareSearch
              players={this.state.playerList4}
              playerClick={this.playerClick4}
              handleSearchChange={this.handleSearchChange4}
            />
          ) : (
            <ComparePlayer
              teams={this.props.teams}
              activePlayer={this.state.activePlayer4}
              activePlayerStats={this.props.player4stats}
              closePlayerWindow={this.closePlayerWindow4}
            />
          )}
        </div>
      </div>
    );
  }
}

export default Compare;
