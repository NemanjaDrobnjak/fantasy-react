import React from "react";

import Teams from "./Teams";
import Header from "./Header";
import TeamPlayers from "./TeamPlayers";
import Player from "./Player";
import Compare from "./Compare";
import Loader from "./Loader";
import Analysts from "./Analysts";
import Social from "./Social";

import { Route, Switch } from "react-router";

import { connect } from "react-redux";
import {
  loadAllData,
  loadPlayerData,
  loadYTVideos,
  toggleLoader,
  loadRedditFeed,
} from "../actions/index";

class Home extends React.Component {
  constructor(props) {
    super();
    this.state = {
      activeComponent: "Teams",
      activeTeamId: 0,
      activeTeam: {},
      activePlayerId: 0,
      activePlayer: {},
    };
    this.changeActiveComponent = this.changeActiveComponent.bind(this);
  }

  componentDidMount() {
    this.props.toggleLoader(true);
    this.props.loadAllData();
  }

  // Switch components
  changeActiveComponent(component) {
    window.scrollTo(0, 0);
    this.setState({
      activeComponent: component,
    });
  }

  render() {
    const {
      players,
      teamsCalculated,
      fixtures,
      player1stats,
      player2stats,
      player3stats,
      player4stats,
      analyst1,
      analyst2,
      analyst3,
      analyst4,
      analyst5,
      loading,
      redditPosts,
    } = this.props;

    return (
      <div className="app__wrapper">
        <Loader loading={loading} />
        <Header />
        <Switch>
          <Route exact path="/">
            <Teams teams={teamsCalculated} />
          </Route>
          <Route
            path={`/team/:teamId`}
            render={(routerProps) => <TeamPlayers {...routerProps} />}
          />
          <Route exact path="/compare">
            <Compare
              teams={teamsCalculated}
              players={players}
              loadPlayerData={this.props.loadPlayerData}
              player1stats={player1stats}
              player2stats={player2stats}
              player3stats={player3stats}
              player4stats={player4stats}
            />
          </Route>
          <Route exact path="/analysts">
            <Analysts
              analyst1={analyst1}
              analyst2={analyst2}
              analyst3={analyst3}
              analyst4={analyst4}
              analyst5={analyst5}
              loadYTVideos={this.props.loadYTVideos}
            />
          </Route>
          <Route exact path="/social">
            <Social redditPosts={redditPosts} loadRedditFeed={this.props.loadRedditFeed} />
          </Route>
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    players: state.allData.players,
    teamsCalculated: state.allData.teams,
    fixtures: state.allData.fixtures,
    player1stats: state.playerData.player1stats,
    player2stats: state.playerData.player2stats,
    player3stats: state.playerData.player3stats,
    player4stats: state.playerData.player4stats,
    analyst1: state.analysts.analyst1,
    analyst2: state.analysts.analyst2,
    analyst3: state.analysts.analyst3,
    analyst4: state.analysts.analyst4,
    analyst5: state.analysts.analyst5,
    loading: state.loader.loading,
    redditPosts: state.reddit.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadAllData: () => dispatch(loadAllData()),
    loadPlayerData: (playerId, playerNumber) =>
      dispatch(loadPlayerData(playerId, playerNumber)),
    toggleLoader: (status) => dispatch(toggleLoader(status)),
    loadYTVideos: () => dispatch(loadYTVideos()),
    loadRedditFeed: () => dispatch(loadRedditFeed()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
