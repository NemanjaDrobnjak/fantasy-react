import React from "react";

class ComparePlayer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="compare__player-container">
        <div className="compare__player-header">
          <div className="header__title">Comparing player...</div>
          <div
            className="compare__player-close"
            onClick={() => {
              this.props.closePlayerWindow();
            }}
          >
            X
          </div>
        </div>
        <div className="compare__player-info">
          <div className="player__name">{this.props.activePlayer.web_name}</div>
          <div className="player__team">
            {this.props.teams.find((team) => team.id === this.props.activePlayer.team).name}
          </div>
        </div>
        <div className="compare__player-stats">
          <div className="stats__header-row">
            <div className="stats__label">Form</div>
            <div className="stats__label">Price</div>
            <div className="stats__label">Selected</div>
            <div className="stats__label">GW</div>
            <div className="stats__label">Total Points</div>
            <div className="stats__label">Bonus Points</div>
            <div className="stats__label">Penalties Order</div>
          </div>
          <div className="stats__row">
            <div className="stats__value">{this.props.activePlayer.form}</div>
            <div className="stats__value">
              £{(this.props.activePlayer.now_cost / 10).toFixed(1)}
            </div>
            <div className="stats__value">
              {this.props.activePlayer.selected_by_percent}%
            </div>
            <div className="stats__value">
              {this.props.activePlayer.event_points}pts
            </div>
            <div className="stats__value">
              {this.props.activePlayer.total_points}pts
            </div>
            <div className="stats__value">{this.props.activePlayer.bonus}</div>
            <div className="stats__value">
              {this.props.activePlayer.penalties_order}
            </div>
          </div>
        </div>
        <div className="compare__player-ict">
          <div className="stats__header-row">
            <div className="stats__label">Influence rank</div>
            <div className="stats__label">Creativity rank</div>
            <div className="stats__label">Threat rank</div>
            <div className="stats__label">ICT Index rank</div>
            <div className="stats__label">ICT Overall rank</div>
          </div>
          <div className="stats__row">
            <div className="stats__value">
              {this.props.activePlayer.influence_rank_type}
            </div>
            <div className="stats__value">
              {this.props.activePlayer.creativity_rank_type}
            </div>
            <div className="stats__value">
              {this.props.activePlayer.threat_rank_type}
            </div>
            <div className="stats__value">
              {this.props.activePlayer.ict_index_rank_type}
            </div>
            <div className="stats__value">
              {this.props.activePlayer.ict_index_rank}
            </div>
          </div>
        </div>
        <div className="compare__previous-year">
          <div className="year__title">2019/20 stats</div>
        </div>
        <div className="compare__player-extra-stats">
          <div className="stats__header-row">
            <div className="stats__label">Points</div>
            <div className="stats__label">Minutes</div>
            <div className="stats__label">Goals</div>
            <div className="stats__label">Assists</div>
            <div className="stats__label">Clean Sheets</div>
            <div className="stats__label">Saves</div>
            <div className="stats__label">Bonus Points</div>
            <div className="stats__label">Price Start</div>
            <div className="stats__label">Price End</div>
          </div>
          <div className="stats__row">
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.total_points}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.minutes}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.goals_scored}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.assists}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.clean_sheets}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.saves}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2019.bonus}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && (this.props.activePlayerStats.year2019.start_cost / 10).toFixed(1)}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && (this.props.activePlayerStats.year2019.end_cost / 10).toFixed(1)}
            </div>
          </div>
        </div>
        <div className="compare__previous-year">
          <div className="year__title">2018/19 stats</div>
        </div>
        <div className="compare__player-extra-stats">
          <div className="stats__header-row">
            <div className="stats__label">Points</div>
            <div className="stats__label">Minutes</div>
            <div className="stats__label">Goals</div>
            <div className="stats__label">Assists</div>
            <div className="stats__label">Clean Sheets</div>
            <div className="stats__label">Saves</div>
            <div className="stats__label">Bonus Points</div>
            <div className="stats__label">Price Start</div>
            <div className="stats__label">Price End</div>
          </div>
          <div className="stats__row">
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.total_points}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.minutes}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.goals_scored}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.assists}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.clean_sheets}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.saves}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && this.props.activePlayerStats.year2018.bonus}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && (this.props.activePlayerStats.year2018.start_cost / 10).toFixed(1)}
            </div>
            <div className="stats__value">
              {this.props.activePlayerStats && (this.props.activePlayerStats.year2018.end_cost / 10).toFixed(1)}
            </div>
          </div>
        </div>
        <div className="compare__next-features"></div>
      </div>
    );
  }
}


export default ComparePlayer;
