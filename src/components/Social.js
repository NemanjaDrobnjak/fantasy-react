import React, { useEffect } from "react";

import { getUniqueKey } from "../helpers/functions";

import { Timeline } from "react-twitter-widgets";

const Social = (props) => {
  useEffect(() => {
    props.loadRedditFeed();
  }, []);

  return (
    <div className="social__container">
      <div className="social__column">
        <div className="column__header">Injury Reports</div>
        <div className="column__element">
          <Timeline
            dataSource={{
              sourceType: "profile",
              screenName: "BenDinnery",
            }}
            options={{ theme: "dark", height: "600" }}
          />
        </div>
      </div>
      <div className="social__column">
        <div className="column__header">FPL Reddit</div>
        <div className="column__element">
          <div className="reddit__element">
            <div className="reddit__header">Reddit by PLFantasy</div>
            <div className="reddit__viewport">
              {props.redditPosts &&
                props.redditPosts.map((post) => {
                  return (
                    <div
                      key={getUniqueKey()}
                      className="reddit__row"
                      onClick={() => {
                        window.open(post.data.url, "_blank");
                      }}
                    >
                      <div className="row__title">{post.data.title}</div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
      <div className="social__column">
        <div className="column__header">Other Tools</div>
        <div className="column__element">
          <div className="reddit__element">
            <div className="reddit__header">Useful Tools List</div>
            <div className="reddit__viewport">
              <div
                className="reddit__row"
                onClick={() => {
                  window.open("https://understat.com/league/EPL", "_blank");
                }}
              >
                <div className="row__title">
                  Understat - Player and Teams Expected Goals, Expected
                  Assists...
                </div>
              </div>

              <div
                className="reddit__row"
                onClick={() => {
                  window.open(
                    "https://myfplanalysis.co.in/understat_player_overview#",
                    "_blank"
                  );
                }}
              >
                <div className="row__title">
                  Player Shots (Outside Box, Penalty Box), Key Passes, Expected
                  Goals ...
                </div>
              </div>
              <div
                className="reddit__row"
                onClick={() => {
                  window.open("https://www.fplgameweek.com/#/", "_blank");
                }}
              >
                <div className="row__title">
                  Compare your team to other teams in your leagues
                </div>
              </div>
              <div
                className="reddit__row"
                onClick={() => {
                  window.open("https://fpl.link/", "_blank");
                }}
              >
                <div className="row__title">Sum of useful FPL links</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Social;
