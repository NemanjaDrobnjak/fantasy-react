import React from "react";

import { Link } from "react-router-dom";

import { Timeline } from "react-twitter-widgets";

import { getUniqueKey } from "../helpers/functions";

class Teams extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div className="teams__container">
        <div className="teams__list">
          <div className="team__row">
            <div className="column__header">Position</div>
            <div className="column__header">Team Name</div>
            <div className="column__header">GP</div>
            <div className="column__header">Win</div>
            <div className="column__header">Draw</div>
            <div className="column__header">Loss</div>
            <div className="column__header">GS</div>
            <div className="column__header">GR</div>
            <div className="column__header">GD</div>
            <div className="column__header">TP</div>
            <div className="column__header">Next Fixtures</div>
          </div>
          {this.props.teams &&
            this.props.teams.map((team) => {
              return (
                <Link
                  key={getUniqueKey()}
                  className="team__row"
                  to={{
                    pathname: `/team/${team.id}`,
                    state: {
                      team: team,
                    },
                  }}
                >
                  <div className="row__entry">
                    {this.props.teams.indexOf(team) + 1}
                  </div>
                  <div className="row__entry">{team.name}</div>
                  <div className="row__entry">
                    {team.win + team.draw + team.loss}
                  </div>
                  <div className="row__entry">{team.win}</div>
                  <div className="row__entry">{team.draw}</div>
                  <div className="row__entry">{team.loss}</div>
                  <div className="row__entry">{team.goal_scored}</div>
                  <div className="row__entry">{team.goal_received}</div>
                  <div className="row__entry">{team.goal_diff}</div>
                  <div className="row__entry">{team.points}</div>
                  <div className="row__entry fixtures">
                    {team.nextFixtures.slice(0, 10).map((fixture) => {
                      let customClassName = "single__fixture " + fixture.rating;
                      return <div className={customClassName}>{fixture.name} ({fixture.stadium})</div>;
                    })}
                  </div>
                </Link>
              );
            })}
        </div>
        <div className="team__social">
          <Timeline
            dataSource={{
              sourceType: "profile",
              screenName: "OfficialFPL",
            }}
            options={{ theme: "dark", width: "400", height: "600" }}
          />
        </div>
      </div>
    );
  }
}

export default Teams;
