import React, { useEffect } from "react";

import { getUniqueKey } from "../helpers/functions";

const Analysts = (props) => {
  useEffect(() => {
    props.loadYTVideos();
  }, []);

  return (
    <div className="analysts__container">
      <div className="analyst__header">FPL Tips</div>
      <div className="analyst__row">
        {props.analyst1.items &&
          props.analyst1.items.map((video) => {
            return (
              <div
                key={getUniqueKey()}
                className="video__thumbnail"
                onClick={() => {
                  window.open(
                    "https://www.youtube.com/watch?v=" +
                      video.contentDetails.videoId,
                    "_blank"
                  );
                }}
              >
                <img
                  className=""
                  src={video.snippet.thumbnails.medium.url}
                ></img>
                <div className="video__title">{video.snippet.title}</div>
              </div>
            );
          })}
      </div>
      <div className="analyst__header">Let's talk FPL</div>
      <div className="analyst__row">
        {props.analyst2.items &&
          props.analyst2.items.map((video) => {
            return (
              <div
                key={getUniqueKey()}
                className="video__thumbnail"
                onClick={() => {
                  window.open(
                    "https://www.youtube.com/watch?v=" +
                      video.contentDetails.videoId,
                    "_blank"
                  );
                }}
              >
                <img
                  className=""
                  src={video.snippet.thumbnails.medium.url}
                ></img>
                <div className="video__title">{video.snippet.title}</div>
              </div>
            );
          })}
      </div>
      <div className="analyst__header">fantasyfootballfix</div>
      <div className="analyst__row">
        {props.analyst3.items &&
          props.analyst3.items.map((video) => {
            return (
              <div
                key={getUniqueKey()}
                className="video__thumbnail"
                onClick={() => {
                  window.open(
                    "https://www.youtube.com/watch?v=" +
                      video.contentDetails.videoId,
                    "_blank"
                  );
                }}
              >
                <img
                  className=""
                  src={video.snippet.thumbnails.medium.url}
                ></img>
                <div className="video__title">{video.snippet.title}</div>
              </div>
            );
          })}
      </div>
      <div className="analyst__header">FPL TV</div>
      <div className="analyst__row">
        {props.analyst4.items &&
          props.analyst4.items.map((video) => {
            return (
              <div
                key={getUniqueKey()}
                className="video__thumbnail"
                onClick={() => {
                  window.open(
                    "https://www.youtube.com/watch?v=" +
                      video.contentDetails.videoId,
                    "_blank"
                  );
                }}
              >
                <img
                  className=""
                  src={video.snippet.thumbnails.medium.url}
                ></img>
                <div className="video__title">{video.snippet.title}</div>
              </div>
            );
          })}
      </div>
      <div className="analyst__header">FPL Mate</div>
      <div className="analyst__row">
        {props.analyst5.items &&
          props.analyst5.items.map((video) => {
            return (
              <div
                key={getUniqueKey()}
                className="video__thumbnail"
                onClick={() => {
                  window.open(
                    "https://www.youtube.com/watch?v=" +
                      video.contentDetails.videoId,
                    "_blank"
                  );
                }}
              >
                <img
                  className=""
                  src={video.snippet.thumbnails.medium.url}
                ></img>
                <div className="video__title">{video.snippet.title}</div>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default Analysts;
