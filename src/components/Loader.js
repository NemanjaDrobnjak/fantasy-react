import React from "react";

class Loader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.loading ? (
          <div className="loader__container">
            <div className="loader">
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Loader;
