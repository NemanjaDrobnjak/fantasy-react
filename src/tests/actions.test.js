// tests/actions.test.js
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";

import MockAdapter from "axios-mock-adapter";
import axios from "axios";
import { loadAllData } from "../actions/index";
import { mockTeams, mockPlayers, mockFixtures } from "../helpers/mockData";

import regeneratorRuntime from "regenerator-runtime";

// initialize middlewares
const middlewares = [thunk];
// initialize MockStore which is only the configureStore method which take middlewares as its parameters
const mockStore = configureStore(middlewares);
// creating a mock instance from the MockAdapter of axios
const mock = new MockAdapter(axios);

const store = mockStore({ teamsCalculated: [] });

describe("Action creators", () => {
  beforeEach(() => {
    store.clearActions();
  });

  it("should load all data", async () => {
    mock
      .onGet(
        `https://cors-anywhere.herokuapp.com/https://fantasy.premierleague.com/api/fixtures/`
      )
      .reply(200, mockPlayers, mockTeams);

    mock
      .onGet(
        `https://cors-anywhere.herokuapp.com/https://fantasy.premierleague.com/api/fixtures/`
      )
      .reply(200, mockFixtures);

    let expectedActions = [
      {
        type: "LOAD_MAIN_DATA",
        fixtures: mockFixtures,
        teams: mockTeams,
        players: mockPlayers,
      },
    ];

    return store.dispatch(loadAllData()).then(() => {
      expectedActions(store.getActions()).toEqual(expectedActions);
    });
  });
});
