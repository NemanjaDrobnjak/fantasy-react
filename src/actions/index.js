import axios from "axios";

import { addPointsAndNextFixturesToTeams, addPlayersToTeams } from "../helpers/functions";

const YT_API_TOKEN = "AIzaSyDLx-0-M2_tKGdUQYvgbkW78zxXyg9KImY";

let axiosConfig = {
  headers: {
    Accept: "application/json",
  },
};

export const loadAllData = () => {
  return async function (dispatch) {
    function getBootstrapData() {
      return axios.get(
        `https://cors-anywhere.herokuapp.com/https://fantasy.premierleague.com/api/bootstrap-static/`
      );
    }

    function getFixturesData() {
      return axios.get(
        `https://cors-anywhere.herokuapp.com/https://fantasy.premierleague.com/api/fixtures/`
      );
    }

    Promise.all([getBootstrapData(), getFixturesData()])
      .then(function (results) {
        let status = false;
        dispatch({ type: "TOGGLE_LOADER", status });
        let bootstrapData = results[0].data;
        let fixtures = results[1].data;

        let teams = bootstrapData.teams;
        let players = bootstrapData.elements;

        teams = addPointsAndNextFixturesToTeams(fixtures, teams);
        teams = addPlayersToTeams(players, teams);

        dispatch({ type: "LOAD_MAIN_DATA", fixtures, teams, players });
      })
      .catch((err) => {
        console.log("ERROR ", err);
      });
  };
};

export const loadPlayerData = (playerId, playerNumber) => {
  return function (dispatch, getState) {
    axios
      .get(
        `https://cors-anywhere.herokuapp.com/https://fantasy.premierleague.com/api/element-summary/` +
          playerId +
          `/`
      )
      .then((res) => {
        let playerData = res.data;
        let year2019 = playerData.history_past.find((year) => {
          return year.season_name === "2019/20";
        });
        let year2018 = playerData.history_past.find((year) => {
          return year.season_name === "2018/19";
        });
        let nextFixtures = [
          playerData.fixtures[0],
          playerData.fixtures[1],
          playerData.fixtures[2],
          playerData.fixtures[3],
          playerData.fixtures[4],
        ];
        let player1stats = getState().playerData.player1stats
          ? getState().playerData.player1stats
          : {};
        let player2stats = getState().playerData.player2stats
          ? getState().playerData.player2stats
          : {};
        let player3stats = getState().playerData.player3stats
          ? getState().playerData.player3stats
          : {};
        let player4stats = getState().playerData.player4stats
          ? getState().playerData.player4stats
          : {};
        // Handle undefined
        if (year2019 === undefined) {
          year2019 = {
            season_name: "2019/20",
            element_code: 0,
            start_cost: 0,
            end_cost: 0,
            total_points: 0,
            minutes: 0,
            goals_scored: 0,
            assists: 0,
            clean_sheets: 0,
            goals_conceded: 0,
            own_goals: 0,
            penalties_saved: 0,
            penalties_missed: 0,
            yellow_cards: 0,
            red_cards: 0,
            saves: 0,
            bonus: 0,
            bps: 0,
            influence: 0,
            creativity: 0,
            threat: 0,
            ict_index: 0,
          };
        }

        if (year2018 === undefined) {
          year2018 = {
            season_name: "2018/19",
            element_code: 0,
            start_cost: 0,
            end_cost: 0,
            total_points: 0,
            minutes: 0,
            goals_scored: 0,
            assists: 0,
            clean_sheets: 0,
            goals_conceded: 0,
            own_goals: 0,
            penalties_saved: 0,
            penalties_missed: 0,
            yellow_cards: 0,
            red_cards: 0,
            saves: 0,
            bonus: 0,
            bps: 0,
            influence: 0,
            creativity: 0,
            threat: 0,
            ict_index: 0,
          };
        }

        if (playerNumber === 1) {
          player1stats = {
            year2018,
            year2019,
            nextFixtures,
          };
        } else if (playerNumber === 2) {
          player2stats = {
            year2018,
            year2019,
            nextFixtures,
          };
        } else if (playerNumber === 3) {
          player3stats = {
            year2018,
            year2019,
            nextFixtures,
          };
        } else {
          player4stats = {
            year2018,
            year2019,
            nextFixtures,
          };
        }
        dispatch({
          type: "LOAD_PLAYER_DATA",
          player1stats,
          player2stats,
          player3stats,
          player4stats,
        });
      })
      .catch((err) => {
        console.log("ERROR ", err);
      });
  };
};

export const loadYTVideos = () => {
  return async function (dispatch) {
    function getAnalyst1() {
      return axios.get(
        `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=10&playlistId=UUVPb_jLxwaoYd-Dm7aSWQKQ&key=` +
          YT_API_TOKEN,
        axiosConfig
      );
    }

    function getAnalyst2() {
      return axios.get(
        `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=10&playlistId=UUxeOc7eFxq37yW_Nc-69deA&key=` +
          YT_API_TOKEN,
        axiosConfig
      );
    }

    function getAnalyst3() {
      return axios.get(
        `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=10&playlistId=UU0Oaf88gRGnNkncI8D_GO-Q&key=` +
          YT_API_TOKEN,
        axiosConfig
      );
    }

    function getAnalyst4() {
      return axios.get(
        `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=10&playlistId=UUfFNeaeLjTAEwpKUUbDwCCg&key=` +
          YT_API_TOKEN,
        axiosConfig
      );
    }

    function getAnalyst5() {
      return axios.get(
        `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=10&playlistId=UUweDAlFm2LnVcOqaFU4_AGA&key=` +
          YT_API_TOKEN,
        axiosConfig
      );
    }

    Promise.all([
      getAnalyst1(),
      getAnalyst2(),
      getAnalyst3(),
      getAnalyst4(),
      getAnalyst5(),
    ])
      .then(function (results) {
        const analyst1 = results[0].data;
        const analyst2 = results[1].data;
        const analyst3 = results[2].data;
        const analyst4 = results[3].data;
        const analyst5 = results[4].data;
        dispatch({
          type: "LOAD_ALL_ANALYSTS",
          analyst1,
          analyst2,
          analyst3,
          analyst4,
          analyst5,
        });
      })
      .catch((err) => {
        console.log("ERROR ", err);
      });
  };
};

export const loadRedditFeed = () => {
  return function (dispatch) {
    let posts;
    axios.get("https://www.reddit.com/r/FantasyPL/hot/.json?count=20").then(
      (res) => {
        posts = res.data.data.children;
        dispatch({type: "LOAD_REDDIT_FEED", posts})
      }
    )
  }
}

export const toggleLoader = (status) => {
  return function (dispatch) {
    dispatch({ type: "TOGGLE_LOADER", status });
  };
};
